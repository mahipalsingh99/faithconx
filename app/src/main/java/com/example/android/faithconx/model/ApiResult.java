package com.example.android.faithconx.model;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

public class ApiResult implements Serializable {
  private List<? extends Results> results;
  public List<? extends Results> getResults() {
    return this.results;
  }

  public void setResults(List<? extends Results> results) {
    this.results = results;
  }

  public static class Results implements Serializable {
    private Name name;
    private Location location;
    private String email;
    private Picture picture;

    public Name getName() {
      return this.name;
    }

    public void setName(Name name) {
      this.name = name;
    }

    public Location getLocation() {
      return this.location;
    }

    public void setLocation(Location location) {
      this.location = location;
    }

    public String getEmail() {
      return this.email;
    }

    public void setEmail(String email) {
      this.email = email;
    }

    public Picture getPicture() {
      return this.picture;
    }

    public void setPicture(Picture picture) {
      this.picture = picture;
    }

    public static class Name implements Serializable {
      private String last;

      private String title;

      private String first;

      public String getLast() {
        return this.last;
      }

      public void setLast(String last) {
        this.last = last;
      }

      public String getTitle() {
        return this.title;
      }

      public void setTitle(String title) {
        this.title = title;
      }

      public String getFirst() {
        return this.first;
      }

      public void setFirst(String first) {
        this.first = first;
      }
    }

    public static class Location implements Serializable {
      private String country;

      private String city;

      public String getCountry() {
        return this.country;
      }

      public void setCountry(String country) {
        this.country = country;
      }

      public String getCity() {
        return this.city;
      }

      public void setCity(String city) {
        this.city = city;
      }

    }

    public static class Picture implements Serializable {
      private String thumbnail;

      private String large;

      private String medium;

      public String getThumbnail() {
        return this.thumbnail;
      }

      public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
      }

      public String getLarge() {
        return this.large;
      }

      public void setLarge(String large) {
        this.large = large;
      }

      public String getMedium() {
        return this.medium;
      }

      public void setMedium(String medium) {
        this.medium = medium;
      }
    }
  }

}
