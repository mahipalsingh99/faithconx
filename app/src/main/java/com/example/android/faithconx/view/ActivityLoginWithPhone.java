package com.example.android.faithconx.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.example.android.faithconx.R;
import com.example.android.faithconx.databinding.ActivityLoginWithPhoneBinding;
import com.example.android.faithconx.viewmodel.LoginWithPhoneViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

//activity for logging in with phone number
public class ActivityLoginWithPhone extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private LoginWithPhoneViewModel loginWithPhoneViewModel;
    private ActivityLoginWithPhoneBinding activityLoginWithPhone;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private String mVerificationId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginWithPhoneViewModel = new LoginWithPhoneViewModel();
        activityLoginWithPhone = DataBindingUtil.setContentView(this, R.layout.activity_login_with_phone);
        activityLoginWithPhone.setPhoneLogin(loginWithPhoneViewModel);
        activityLoginWithPhone.executePendingBindings();
        //for closing the activity
        activityLoginWithPhone.ivClose.setOnClickListener((View v)->{
            finish();
        });
        mAuth = FirebaseAuth.getInstance();

        activityLoginWithPhone.btnRequestOtp.setOnClickListener((View v)->{
            requestVerificationCode(activityLoginWithPhone.etPhone.getText().toString().trim());
        });
        //phone validation
        activityLoginWithPhone.etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(Pattern.matches("[1-9][0-9]{9}",s)){
                    activityLoginWithPhone.btnRequestOtp.setEnabled(true);
                }
                else{
                    activityLoginWithPhone.btnRequestOtp.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //otp validation
        activityLoginWithPhone.etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 6){
                    activityLoginWithPhone.btnSubmitOtp.setEnabled(true);
                }
                else{
                    activityLoginWithPhone.btnSubmitOtp.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }
    //method to get otp on provided phone no.
    private void requestVerificationCode(String phone) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+91" + phone,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallBack);

    }
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        //storing unique id to string
        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            mVerificationId = s;
            mResendToken = forceResendingToken;
        }
        //setting code on edit text after getting otp
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if(code != null){
                activityLoginWithPhone.etOtp.setText(code);
                activityLoginWithPhone.btnSubmitOtp.setOnClickListener((View v)->{
                    verifyCode(code);
                });

            }
        }
        //toast message for user if verification failed
        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };
    //finishing current activity if back is pressed
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    //method to verify if the code is correct
    private void verifyCode(String code){
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId,code);
        signInWithPhoneAuthCredential(credential);
    }
    //redirecting user to another activity if the verification is successful
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential){
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(ActivityLoginWithPhone.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            Toast.makeText(getApplicationContext(), "login successful", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ActivityLoginWithPhone.this, PhoneLoginUserDetailsActivity.class));
                            finish();
                        }
                        else{
                            Toast.makeText(getApplicationContext(), "login fail", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }
}