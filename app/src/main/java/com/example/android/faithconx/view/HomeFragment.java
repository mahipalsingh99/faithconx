package com.example.android.faithconx.view;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.android.faithconx.MyAdapter;
import com.example.android.faithconx.databinding.FragmentHomeBinding;
import com.example.android.faithconx.model.ApiResult;
import com.example.android.faithconx.model.UserInfo;
import com.example.android.faithconx.network.RetrofitInstance;
import com.example.android.faithconx.utils.Constants;
import com.example.android.faithconx.viewmodel.HomeFragmentViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//fragment for displaying api data in recycler view on app start
public class HomeFragment extends Fragment {
    private FragmentHomeBinding fragmentHomeBinding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentHomeBinding = FragmentHomeBinding.inflate(inflater,container,false);
        View view = fragmentHomeBinding.getRoot();
        //progress bar
        Animation animation = new ProgressAnimation(Constants.FROM,Constants.TO);
        fragmentHomeBinding.pbProgressBar.setAnimation(animation);
        //api call
        HomeFragmentViewModel homeFragmentViewModel = new HomeFragmentViewModel();
        List<UserInfo> userInfoList = new ArrayList<>();
        homeFragmentViewModel.getList(getContext());
        MyAdapter adapter = new MyAdapter(userInfoList, getContext());
        fragmentHomeBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        fragmentHomeBinding.recyclerView.setAdapter(adapter);
        homeFragmentViewModel.getUserInfoList().observe(getViewLifecycleOwner(), userInfos -> {
            userInfoList.clear();
            userInfoList.addAll(userInfos);
            fragmentHomeBinding.pbProgressBar.setVisibility(View.GONE);
            adapter.notifyDataSetChanged();
        });
        return view;
    }
    //class for progress bar
    class ProgressAnimation extends Animation{
        private int from;
        private int to;
        ProgressAnimation(int from,int to){
            this.from = from;
            this.to = to;
        }
        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            float value = from + (to-from)*interpolatedTime;
            fragmentHomeBinding.pbProgressBar.setProgress((int)value);
        }
    }
}

