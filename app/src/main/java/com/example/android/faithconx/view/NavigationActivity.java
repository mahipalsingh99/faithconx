package com.example.android.faithconx.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.android.faithconx.R;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
//activity containing natigation bar
public class NavigationActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    BottomNavigationView btmNavigation;
    HomeFragment homeFragment = new HomeFragment();
    ProfileFragment profileFragment = new ProfileFragment();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        //default view on home screen
        btmNavigation = findViewById(R.id.bottom_navigation_bar);
        btmNavigation.setOnNavigationItemSelectedListener(this);
        btmNavigation.setSelectedItemId(R.id.bn_home);
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.bn_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment, homeFragment).commit();
                return true;
            case R.id.bn_profile:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment, profileFragment).commit();
                return true;
        }
        return false;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}