package com.example.android.faithconx.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.example.android.faithconx.R;
import com.example.android.faithconx.databinding.ActivityLoginWithPhoneBinding;
import com.example.android.faithconx.databinding.ActivityPhoneLoginUserDetailsBinding;
import com.example.android.faithconx.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
//getting user details and storing to firebase realtime after log in with phone number
public class PhoneLoginUserDetailsActivity extends AppCompatActivity {
    ActivityPhoneLoginUserDetailsBinding activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = ActivityPhoneLoginUserDetailsBinding.inflate(getLayoutInflater());
        View view = activity.getRoot();
        setContentView(view);
        activity.btnSubmit.setOnClickListener((View v)->{
            String firstName = activity.etFirstName.getText().toString().trim();
            String lastName = activity.etLastName.getText().toString().trim();
            String email = activity.etEmail.getText().toString().trim();
            User user = new User();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            //adding data to realtime database
            FirebaseDatabase.getInstance().getReference().child("users").child(uid).setValue(user);
            startActivity(new Intent(PhoneLoginUserDetailsActivity.this, NavigationActivity.class));
            finish();
        });
    }
}