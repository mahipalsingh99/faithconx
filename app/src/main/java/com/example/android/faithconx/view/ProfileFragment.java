package com.example.android.faithconx.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.android.faithconx.R;
import com.example.android.faithconx.databinding.FragmentProfileBinding;
import com.example.android.faithconx.model.User;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

//fragment for profile of user
public class ProfileFragment extends Fragment {
    FirebaseDatabase rootNode;
    DatabaseReference reference;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentProfileBinding fragmentProfileBinding = FragmentProfileBinding.inflate(inflater,container,false);
        View view = fragmentProfileBinding.getRoot();
        fragmentProfileBinding.ivSettings.setOnClickListener((View v)->{
            if(fragmentProfileBinding.btnLogout.getVisibility() == View.GONE)
                fragmentProfileBinding.btnLogout.setVisibility(View.VISIBLE);
            else
                fragmentProfileBinding.btnLogout.setVisibility(View.GONE);
        });
        fragmentProfileBinding.btnLogout.setOnClickListener((View v)->{
            FirebaseAuth.getInstance().signOut();
            GoogleSignInClient mGoogleSignInClient= GoogleSignIn.getClient(getContext(),new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build());
            // Google sign out
            mGoogleSignInClient.signOut().addOnCompleteListener(task -> {
                startActivity(new Intent(getContext(), SignInActivity.class));
                getActivity().finish();
            });
        });
        //set user data in profile
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        rootNode = FirebaseDatabase.getInstance("https://fir-withfaithconx-default-rtdb.firebaseio.com/");
        if(currentUser != null){
            reference = rootNode.getReference().child("users").child(currentUser.getUid());
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    fragmentProfileBinding.tvProfileName.setText(user.getFirstName()+" "+user.getLastName());
                    fragmentProfileBinding.tvProfileId.setText(user.getEmail());
                    fragmentProfileBinding.about.setText(user.getPhoneNumber());
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Toast.makeText(getContext(),getText( R.string.dataNotRetrieved), Toast.LENGTH_SHORT).show();
                }
            });
        }
        else{
            Toast.makeText(getContext(), getText(R.string.loginAgain), Toast.LENGTH_SHORT).show();
        }
        return view;
    }
}