package com.example.android.faithconx.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.android.faithconx.R;
import com.example.android.faithconx.databinding.ActivitySignInBinding;
import com.example.android.faithconx.model.User;
import com.example.android.faithconx.utils.Constants;
import com.example.android.faithconx.viewmodel.SignInViewModel;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Pattern;
//activity for sign in of user
public class SignInActivity extends AppCompatActivity {
    GoogleSignInClient mGoogleSignInClient;
    private ActivitySignInBinding activitySignInBinding;
    private FirebaseAuth mAuth;
    private SignInViewModel signInViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        signInViewModel = new SignInViewModel();
        activitySignInBinding = DataBindingUtil.setContentView(this,R.layout.activity_sign_in);
        activitySignInBinding.setSignIn(signInViewModel);
        activitySignInBinding.executePendingBindings();
        //for closing the activity
        activitySignInBinding.ivClose.setOnClickListener((View v)->{
            finish();
        });
        //redirecting user to sign up page
        activitySignInBinding.tvCreateAccount.setOnClickListener((View v)->{
            startActivity(new Intent(getApplicationContext(),SignUpActivity.class));
        });
        // Initialize Firebase Auth
        FirebaseApp.initializeApp(getApplicationContext());
        mAuth = FirebaseAuth.getInstance();
        //sending user email for forgot password
        activitySignInBinding.tvForgotPassword.setOnClickListener((View v)->{
            String userEmail = activitySignInBinding.etSigninEmail.getText().toString().trim();
            if(userEmail.length() != 0){
                FirebaseAuth.getInstance().sendPasswordResetEmail(userEmail)
                        .addOnCompleteListener((@NonNull Task<Void> task)->{
                            if(task.isSuccessful()){
                                Toast.makeText(getApplicationContext(), R.string.emailSent, Toast.LENGTH_LONG).show();
                            }
                            else{
                                Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
                            }
                        });
            }
            else{
                Toast.makeText(getApplicationContext(), R.string.enterEmailAndPass, Toast.LENGTH_SHORT).show();
            }
        });
        //password visibility
        activitySignInBinding.ivPasswordVisibilityOff.setOnClickListener((View v)->{
            activitySignInBinding.ivPasswordVisibilityOn.setVisibility(View.VISIBLE);
            activitySignInBinding.ivPasswordVisibilityOff.setVisibility(View.GONE);
            activitySignInBinding.etSigninPassword.setTransformationMethod(null);
        });
        activitySignInBinding.ivPasswordVisibilityOn.setOnClickListener((View v)->{
            activitySignInBinding.ivPasswordVisibilityOff.setVisibility(View.VISIBLE);
            activitySignInBinding.ivPasswordVisibilityOn.setVisibility(View.GONE);
            activitySignInBinding.etSigninPassword.setTransformationMethod(new PasswordTransformationMethod());
        });
        //Google signIn or singUp for user with firebase
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        //GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        activitySignInBinding.btnGoogleLogin.setOnClickListener((View v)->{
            signIn();
        });
        //login with phone number
        activitySignInBinding.btnPhoneNumberLogin.setOnClickListener((View v)->{
            startActivity(new Intent(this, ActivityLoginWithPhone.class));
        });
        //email verification
        activitySignInBinding.btnLogIn.setOnClickListener((View v)->{
            String email = activitySignInBinding.etSigninEmail.getText().toString().trim();
            String password = activitySignInBinding.etSigninPassword.getText().toString().trim();
            if(!Pattern.matches("[a-zA-Z0-9]+@[a-zA-Z0-9]+[.][a-zA-Z]{2,3}",email)){
                activitySignInBinding.etSigninEmail.setError("Enter a valid Email");
            }
            else if(!Pattern.matches("[0-9a-zA-Z&$@#*]{6,20}",password)){
                activitySignInBinding.etSigninPassword.setError("Enter a valid password(between 6-20 characters)");
            }
            else{
                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener((@NonNull Task<AuthResult> task) -> {
                            if (task.isSuccessful()) {
                                startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.incorrectEmailPass, Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });

    }
    //method for google sign in intent
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, Constants.RC_SIGN_IN);
    }
    //checking if the user is already logged in on the start of activity
    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            startActivity(new Intent(getApplicationContext(),NavigationActivity.class));
            finish();
        }
    }
    //handling the sign in from google on return of intent result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }
    //getting account details on successful signin
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            Log.d("google sign in","successful");
            GoogleSignInAccount acc = completedTask.getResult(ApiException.class);
            firebaseAuthenticationWithGoogleAccount(acc);
        } catch (ApiException e) {
            Log.d("google sign in","failed");
            e.printStackTrace();
        }
    }
    //adding google signin details to firebase authentication
    private void firebaseAuthenticationWithGoogleAccount(GoogleSignInAccount acc) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acc.getIdToken(),null);
        mAuth.signInWithCredential(credential)
                .addOnSuccessListener((AuthResult authResult)->{
                    FirebaseUser firebaseUser = mAuth.getCurrentUser();
                    //adding data to realtime database if it is first time login using google
                    if(authResult.getAdditionalUserInfo().isNewUser()) {
                        String uid = firebaseUser.getUid();
                        String email = firebaseUser.getEmail();
                        String phone = firebaseUser.getPhoneNumber();
                        String personGivenName = acc.getGivenName();
                        String personFamilyName = acc.getFamilyName();
                        User user = new User();
                        user.setFirstName(personGivenName);
                        user.setLastName(personFamilyName);
                        user.setEmail(email);
                        user.setPhoneNumber(phone);
                        //adding data to realtime database
                        FirebaseDatabase.getInstance().getReference().child("users").child(uid).setValue(user);
                    }
                    startActivity(new Intent(getApplicationContext(),NavigationActivity.class));
                })
                .addOnFailureListener((@NonNull Exception e)->{
                    e.printStackTrace();
                });
    }
}