package com.example.android.faithconx.view;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.android.faithconx.R;
import com.example.android.faithconx.databinding.ActivitySignInBinding;
import com.example.android.faithconx.databinding.ActivitySignUpBinding;
import com.example.android.faithconx.viewmodel.SignUpViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Pattern;
//activity for sign up of user
public class SignUpActivity extends AppCompatActivity {
    private SignUpViewModel signUpViewModel;
    private FirebaseAuth mAuth;
    private ActivitySignUpBinding activitySignUpBinding;
    private FirebaseDatabase rootNode;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        signUpViewModel = new SignUpViewModel(this);
        //setContentView(R.layout.activity_sign_up);
        activitySignUpBinding = DataBindingUtil.setContentView(this,R.layout.activity_sign_up);
        activitySignUpBinding.setSignUp(signUpViewModel);
        activitySignUpBinding.executePendingBindings();
        // Initialize Firebase Auth
        //FirebaseApp.initializeApp(getApplicationContext());
        FirebaseApp.initializeApp(getApplicationContext());
        mAuth = FirebaseAuth.getInstance();
        activitySignUpBinding.btnContinue.setOnClickListener((View v)->{
            String email = activitySignUpBinding.etEmail.getText().toString().trim();
            String password  = activitySignUpBinding.etPassword.getText().toString().trim();
            String firstName = activitySignUpBinding.etFirstName.getText().toString().trim();
            String lastName = activitySignUpBinding.etLastName.getText().toString().trim();
            String phone = activitySignUpBinding.etPhone.getText().toString().trim();
            String confirmPassword = activitySignUpBinding.etConfirmPassword.getText().toString().trim();
            //applying validations on each field
            if(!Pattern.matches("[a-zA-Z ]{2,}",firstName)){
                activitySignUpBinding.etFirstName.setError(getText(R.string.validFirstName));
            }
            else if(!Pattern.matches("[a-zA-Z]{2,}",lastName)){
                activitySignUpBinding.etLastName.setError(getString(R.string.validLastName));
            }
            else if(!Pattern.matches("[a-zA-Z0-9]+@[a-zA-Z0-9]+[.][a-zA-Z]{2,3}",email)){
                activitySignUpBinding.etEmail.setError(getString(R.string.validEmail));
            }
            else if(!Pattern.matches("[0-9]{10}",phone)){
                activitySignUpBinding.etPhone.setError(getString(R.string.validPhoneNumber));
            }
            else if(!Pattern.matches("[0-9a-zA-Z&$@#*]{6,20}",password)){
                activitySignUpBinding.etPassword.setError(getText(R.string.validPass));
            }
            else if(!Pattern.matches("[0-9a-zA-Z&$@#*]{6,20}",confirmPassword)){
                activitySignUpBinding.etConfirmPassword.setError(getText(R.string.validPass));
            }
            else if(!password.equals(confirmPassword)){
                activitySignUpBinding.etConfirmPassword.setError(getString(R.string.confirmPassword));
            }
            //if all fields are validated
            else{
                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this, task -> {
                            if (task.isSuccessful()) {
                                //inserting in database
                                signUpViewModel.insertUser(task);
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "createUserWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                startActivity(new Intent(getApplicationContext(), NavigationActivity.class));
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                Toast.makeText(getApplicationContext(), getText(R.string.authenticationFailed),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });

        //close
        activitySignUpBinding.ivClose.setOnClickListener((View v)->{
            finish();
        });
        //password visibility
        activitySignUpBinding.ivPasswordVisibilityOff.setOnClickListener((View v)->{
            activitySignUpBinding.ivPasswordVisibilityOn.setVisibility(View.VISIBLE);
            activitySignUpBinding.ivPasswordVisibilityOff.setVisibility(View.GONE);
            activitySignUpBinding.etPassword.setTransformationMethod(null);
        });
        activitySignUpBinding.ivPasswordVisibilityOn.setOnClickListener((View v)->{
            activitySignUpBinding.ivPasswordVisibilityOff.setVisibility(View.VISIBLE);
            activitySignUpBinding.ivPasswordVisibilityOn.setVisibility(View.GONE);
            activitySignUpBinding.etPassword.setTransformationMethod(new PasswordTransformationMethod());
        });
        //confirm password
        activitySignUpBinding.ivConfirmPasswordVisibilityOff.setOnClickListener((View v)->{
            activitySignUpBinding.ivConfirmPasswordVisibilityOn.setVisibility(View.VISIBLE);
            activitySignUpBinding.ivConfirmPasswordVisibilityOff.setVisibility(View.GONE);
            activitySignUpBinding.etConfirmPassword.setTransformationMethod(null);
        });
        activitySignUpBinding.ivConfirmPasswordVisibilityOn.setOnClickListener((View v)->{
            activitySignUpBinding.ivConfirmPasswordVisibilityOff.setVisibility(View.VISIBLE);
            activitySignUpBinding.ivConfirmPasswordVisibilityOn.setVisibility(View.GONE);
            activitySignUpBinding.etConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());
        });
        //terms and condition checkbox
        activitySignUpBinding.cbTerms.setOnClickListener((View v)->{
            activitySignUpBinding.btnContinue.setEnabled(activitySignUpBinding.cbTerms.isChecked());
        });
    }
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            startActivity(new Intent(getApplicationContext(),NavigationActivity.class));
            finish();
        }
    }


}