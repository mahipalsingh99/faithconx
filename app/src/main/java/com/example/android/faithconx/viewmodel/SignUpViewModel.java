package com.example.android.faithconx.viewmodel;

import android.content.Context;
import android.util.Log;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.example.android.faithconx.model.User;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class SignUpViewModel extends BaseObservable {
    private FirebaseDatabase rootNode;
    private DatabaseReference reference;
    private User user;
    private Context context;

    public SignUpViewModel(Context context){
        this.context = context;
        this.user = new User();
    }
    @Bindable
    public String getEmail() {
        return user.getEmail();
    }
    public void setEmail(String email) {
        user.setEmail(email);
    }
    @Bindable

    public String getPassword() {
        return user.getPassword();
    }

    public void setPassword(String password) {
        user.setPassword(password);
    }
    @Bindable
    public String getFirstName() {
        return user.getFirstName();
    }

    public void setFirstName(String firstName) {
        user.setFirstName(firstName);
    }
    @Bindable
    public String getLastName() {
        return user.getLastName();
    }

    public void setLastName(String lastName) {
        user.setLastName(lastName);
    }
    @Bindable
    public String getPhoneNumber() {
        return user.getPhoneNumber();
    }

    public void setPhoneNumber(String phoneNumber) {
        user.setPhoneNumber(phoneNumber);
    }
    //inserting data in database
    public void insertUser(Task<AuthResult> task){
        rootNode = FirebaseDatabase.getInstance("https://fir-withfaithconx-default-rtdb.firebaseio.com/");
        reference = rootNode.getReference();
        reference.child("users").child(task.getResult().getUser().getUid()).setValue(user);
        Log.d("uid",task.getResult().getUser().getUid());
        Log.d("insert user","run"+user.getEmail());

    }
}
